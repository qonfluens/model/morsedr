# LOAD DATA
library(morseDR)
data("cadmium2")
Cd2 <- cadmium2
data("cadmium_daphnia")
Cdd <- cadmium_daphnia

# LOAD FIT
fit_sCd2 <- readRDS(test_path("fixtures", "fit_sCd2.rds"))
fit_rCd2 <- readRDS(test_path("fixtures", "fit_rCd2.rds"))
fit_gCdd <- readRDS(test_path("fixtures", "fit_gCdd.rds"))
