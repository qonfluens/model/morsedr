% Generated by roxygen2: do not edit by hand
% Please edit documentation in R/survData.R
\name{check}
\alias{check}
\alias{check_concentration}
\alias{check_time}
\alias{check_Nsurv}
\alias{check_TimeNsurv}
\alias{check_concNsurv}
\alias{check_tab}
\title{A set of function to check the formatting of data}
\usage{
check_concentration(data)

check_time(data)

check_Nsurv(data)

check_TimeNsurv(data)

check_concNsurv(data)

check_tab(tb, check, msg)
}
\arguments{
\item{data}{a data.frame}

\item{msg}{a message to add to the data.frame}

\item{cN_check}{a list of binary TRUE or FALSE}
}
\description{
check if the \code{concentration} within a \code{time serie} is
numeric and always positive.

check if the \code{time} within a \code{time serie} is (1) numeric,
(2) unique, (3) minimal value is 0.

check if the \code{Nsurv} within a \code{time serie} is (1) integer and
(2) always positive

check if the pair \code{time} - \code{Nsurv} within a \code{time serie}
satisfies (1) Nsurv at t=0 is >0, (2) decreasing

check if the pair \code{conc} - \code{Nsurv} within a \code{time serie}
satisfies that the timeline of concentration covers timeline of Nsurv.

append for data.frame
}
