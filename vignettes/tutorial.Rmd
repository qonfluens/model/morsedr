---
title: "Tutorial"
output: rmarkdown::html_vignette
vignette: >
  %\VignetteIndexEntry{Tutorial}
  %\VignetteEngine{knitr::rmarkdown}
  %\VignetteEncoding{UTF-8}
---

```{r, include = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>"
)
```

```{r setup}
library(morseDR)
```

The package `morseDR` is devoted to the analysis of data from standard toxicity
tests at Target Time. It provides a simple workflow to explore/visualize
a data set, and compute estimations of risk assessment indicators.
This document illustrates a typical use of `morseDR` on survival and 
reproduction data, which can be followed step-by-step to analyze new data sets.

# Survival data analysis

The following example shows all the steps to perform survival analysis on
standard toxicity test data and to produce estimated values of the $LC_x$.
We will use a data set of the library named `cadmium2`, which contains both
survival and reproduction data from a chronic laboratory toxicity test. In this
experiment, snails were exposed to six concentrations of a metal contaminant
(cadmium) during 56 days.

## Step 1: check the structure and the data set integrity

The data from a survival toxicity test should be gathered in a `data.frame` with a
specific layout. This is documented in the paragraph on `survData` in the
reference manual, and you can also inspect one of the data sets provided in
the package (e.g., `cadmium2`). First, we load the data set and use the function
`survDataCheck()` to check that it has the expected layout:

```{r step1TT}
data(cadmium2)
survDataCheck(cadmium2)
```

The output `## No message` just informs that the data set is well-formed.

## Step 2: create a `survData` object

The class `survData` corresponds to \emph{validated} survival data and is the
basic layout used for the subsequent operations. Note that if the
call to `survDataCheck()` reports no error (i.e., `## No message`), it is guaranteed that `survData`
will not fail.

```{r step2TT}
survData_Cd <- survData(cadmium2)
head(survData_Cd)
```

## Step 3: visualize your data set

The function `plot()` can be used to plot the number of surviving individuals
as a function of time for all concentrations and replicates.

```{r step3TT}
plot(survData_Cd, pool.replicate = FALSE)
```


If argument `pool.replicate`
is `TRUE`, datapoints at a given time-point and a given concentration are pooled
and only the mean number of survivors is plotted. To observe the full
data set, we set this option to `FALSE`.

By fixing the concentration at a (tested) value, we can visualize one subplot
in particular:

```{r plotTT}
plot(survData_Cd, concentration = 124,
     addlegend = FALSE,
     pool.replicate = FALSE)
```

We can also pool replicates. In this situation, there is no more legend.

```{r plotTTpool}
plot(survData_Cd,
     pool.replicate = TRUE)
```


We can also plot the survival rate, at a given time-point, as a function of 
concentration, with binomial confidence intervals around the data. This is 
achieved by using function `plotDoseResponse()` and by fixing the option 
`target.time` (default is the end of the experiment).

```{r doseResponse_}
survData_Cd_DR_ <- doseResponse(survData_Cd, target.time = 21)
```

```{r plot_DoseResponse_}
plot(survData_Cd_DR_)
```


```{r doseResponse}
survData_Cd_DR <- doseResponse(survData_Cd, target.time = 21, pool.replicate = TRUE)
```

```{r plot_DoseResponse}
plot(survData_Cd_DR)
```

```{r plot_DoseResponseNoLegend}
plot(survData_Cd_DR, addlegend = FALSE)
```

```{r plot_DoseResponseTT}
plot(survData_Cd_DR, target.time = 21, addlegend = TRUE)
```

Function `summary()` provides some descriptive statistics on the experimental design.

```{r summary}
summary(survData_Cd)
```

## Step 4: fit an exposure-response model to the survival data at target time

Now we are ready to fit a probabilistic model to the survival data, in order to
describe the relationship between the concentration in chemical compound and survival rate
at the target time. Our model assumes this latter is a log-logistic function of
the former, from which the package delivers estimates of the parameters.

We can use the `fit` function, that would recognize the class of the `SurvData` object.

```{r step4, results="hide"}
survFit_Cd <- fit(survData_Cd,
                 target.time = 21,
                 lcx = c(10, 20, 30, 40, 50))
```


```{r summarySurvFit}
summary(survFit_Cd)
```

Once we have estimated the parameters, we can then calculate the $LC_x$ values
for any $x$.

Also, for consistency with the former `morse`package, all this work is 
performed by the `survFitTT()` function, which requires a `SurvData` object as 
input and the levels of $LC_x$ we want:


```{r step4TT, results="hide"}
survFitTT_Cd <- survFitTT(survData_Cd,
                 target.time = 21,
                 lcx = c(10, 20, 30, 40, 50))
```

The returned value is an object of class `survFitTT` providing the estimated
parameters as a posterior[^1] distribution, which quantifies the uncertainty on
their true value. For the parameters of the models, as well as for the $LC_x$
values, we report the median (as the point estimated value) and the 2.5 \% and 97.5 \%
quantiles of the posterior (as a measure of uncertainty, a.k.a. credible
intervals). They can be obtained by using the `summary()` method:

```{r summarySurvFitTT}
summary(survFitTT_Cd)
```

## Plot fitting results

If the inference went well, it is expected that the difference between
quantiles in the posterior will be reduced compared to the prior, meaning
that the data were helpful to reduce the uncertainty on the true value of
the parameters. This simple check can be performed using the summary function.

The fit can also be plotted:

```{r step4plot}
plot(survFit_Cd, log.scale = TRUE, adddata = TRUE,  addlegend = TRUE)
```

```{r step4TTplot}
plot(survFitTT_Cd, log.scale = TRUE, adddata = TRUE,   addlegend = TRUE)
```

This representation shows the estimated relationship between concentration of
chemical compound and survival rate (orange curve). It is computed by choosing for each
parameter the median value of its posterior. To assess the uncertainty on this
estimation, we compute many such curves by sampling the parameters
in the posterior distribution. This gives rise to the grey band, showing for
any given concentration an interval (called credible interval) containing the
survival rate 95% of the time in the posterior distribution. The experimental
data points are represented in black and correspond to the observed survival
rate when
pooling all replicates. The black error bars correspond to a 95% confidence
interval, which is another, more straightforward way to bound the most
probable value of the survival rate for a tested concentration. In favorable
situations, we expect that the credible interval around the estimated curve
and the confidence interval around the experimental data largely overlap.

Note that `survFitTT()` will warn you if the estimated $LC_{x}$ lie outside the
range of tested concentrations, as in the following example:

```{r wrongTT, results="hide"}
data("cadmium1")
doubtful_fit <- fit(survData(cadmium1),
                       target.time = 21,
                       lcx = c(10, 20, 30, 40, 50))
```

```{r wrongTTplot}
plot(doubtful_fit, log.scale = TRUE, style = "ggplot", adddata = TRUE,
     addlegend = TRUE)
```


In this example, the experimental design does not include sufficiently high
concentrations, and we are missing measurements that would have a major
influence on the final estimation. For this reason this result should be
considered unreliable.


## Step 5: validate the model with a posterior predictive check

The fit can be further validated using so-called posterior
predictive checks: the idea is to plot the observed values against the
corresponding estimated predictions, along with their 95% credible
interval. If the fit is correct, we expect to see 95% of the data
inside the intervals.

```{r step5TT, results="hide"}
survFit_Cd_PPC <- ppc(survFit_Cd)
```

```{r plotSurvPPC}
plot(survFit_Cd_PPC)
```

In this plot, each black dot represents an observation made at a given
concentration, and the corresponding number of survivors at target time is given by
the value on the *x-axis*. Using the concentration and the fitted model, we
can produce the corresponding prediction of the expected number of
survivors at that concentration. This prediction is given by the
*y-axis*. Ideally observations and predictions should coincide, so we'd
expect to see the black dots on the points of coordinate $Y = X$. Our
model provides a tolerable variation around the predited mean value
as an interval where we expect 95% of the dots to be in average. The
intervals are represented in green if they overlap with the line $Y=X$,
and in red otherwise.

# Reproduction data analysis

The steps for reproduction data analysis are absolutely analogous to what we
described for survival data. Here, the aim is to estimate the relationship
between the chemical compound concentration and the reproduction rate per individual-day.

Here is a typical session:
```{r reproductionData}
# (1) load data set
data(cadmium2)

# (2) check structure and integrity of the data set
reproDataCheck(cadmium2)

# (3) create a `reproData` object
dat <- reproData(cadmium2)

# (4) represent the cumulated number of offspring as a function of time
plot(dat, concentration = 124, addlegend = TRUE, pool.replicate = FALSE)

# (5) represent the reproduction rate as a function of concentration
dat_DR <- doseResponse(dat, target.time = 28)
plot(dat_DR)

# (6) check information on the experimental design
summary(dat)

# (7) fit a concentration-effect model at target-time
reproFit <- fit(dat, stoc.part = "bestfit",
                  target.time = 21,
                  ecx = c(10, 20, 30, 40, 50),
                  quiet = TRUE)
summary(reproFit)
```

```{r plotReproduction}
plot(reproFit, log.scale = TRUE, adddata = TRUE,
     cicol = "orange",
     addlegend = TRUE)
```

```{r plotReproPPC}
ppc(reproFit)
```

As in the survival analysis, we assume that the reproduction rate per individual-day
is a log-logistic function of the concentration. More details and parameter
signification can be found in the vignette *Models in 'morse' package*.

## Model comparison

For reproduction analyses, we compare one model which neglects the inter-individual
variability (named "Poisson") and another one which takes it into account
(named "gamma Poisson"). You can choose either one or the other with the option `stoc.part`.
Setting this option to `"bestfit"`, you let `reproFitTT()` decides which models fits the
data best. The corresponding choice can be seen by calling the `summary` function:

```{r summaryReproduction}
summary(reproFit)
```
When the gamma Poisson model is selected, the summary shows an additional
parameter called `omega`, which quantifies the inter-individual variability
(the higher `omega` the higher the variability).


## Reproduction data and survival functions

In `morse`, reproduction data sets are a special case of survival data sets: a
reproduction data set includes the same information as in a survival data set plus
the information on reproduction outputs. For that reason, the S3 class `reproData`
inherits from the class `survData`, which means that any operation on a `survData`
object is legal on a `reproData` object. In particular, in order to use the plot function
related to the survival analysis on a `reproData` object, we can use `survData` as a
conversion function first:

```{r reproData}
dat <- reproData(cadmium2)
plot(survData(dat))
```


[^1]: In Bayesian inference, the parameters of a model are estimated
from the data starting from a so-called *prior*, which is a probability
distribution representing an initial guess on the true parameters, before
seeing the data. The *posterior* distribution represents the uncertainty on
the parameters after seeing the data and combining them with the prior. To obtain
a point estimate of the parameters, it is typical to compute the mean or median
of the posterior. We can quantify the uncertainty by reporting the standard
deviation or an inter-quantile distance from this posterior distribution.


# Growth data analysis

This tool provides a dose-response (DR) analysis of growth toxicity test data 
under a Bayesian framework, including an estimation of the x% effective toxicity 
value, that can be an x% effective rate (ERx), an x% effective concentration
(ECx) or any other expression of your choice.

## Load growth data

You can have access to already implemented datasets with the `data` function.
The `growthData` transform a data frame into a `GrowthData`object after making
all checks require for growth analysis.

```{r growthData}
data("cadmium_daphnia")
gCdd <- growthData(cadmium_daphnia)
```

As for other analysis (survival and reproduction), you can check the validity of the 
data set by doing a `growthDataCheck`.

```{r growthDataCheck}
growthDataCheck(cadmium_daphnia)
```

A ploting the dataset with different options: 

```{r plotGrowthData}
plot(gCdd)
```

```{r plotGrowthDataConc}
plot(gCdd, concentration = 25)
```

```{r plotGrowthDataNoLegend}
plot(gCdd, addlegend = TRUE)
```

## DoseResponse checking of growth data

Before the inference, you can run a `t.test` on the data you upload using the
function `doseResponse`. 

```{r DoseResponse}
gCdd_DR <- doseResponse(gCdd)
plot(gCdd_DR)
```

By default, the dose response is applied at the final time (maximal time in the
data set). However, you can specify an another `target.time`.

```{r DoseResponseTT}
gCdd_DRTT <- doseResponse(gCdd, target.time = 7)
plot(gCdd_DRTT)
```

## Inference process on groth data

The fitting process is done using the `fit` function:

```{r growthFit}
fit_gCdd <- fit(gCdd)
```

And you can also plot the results

```{r plotGrowthFit}
plot(fit_gCdd, adddata = TRUE)
```

## Posterior Predictive Check (PPC)

```{r growthPPC}
ppc_gCdd <- ppc(fit_gCdd)
```

That you can plot afterward.

```{r plot_growthPPC}
plot(ppc_gCdd)
```

