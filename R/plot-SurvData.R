#' @title Plotting method for \code{survData} objects
#'
#' @description
#' This is the generic \code{plot} S3 method for the \code{survData} class.
#' It plots the number of survivors as a function of time.
#'
#' @param x an object of class \code{survData}
#' @param xlab a label for the \eqn{X}-axis, by default \code{Time}
#' @param ylab a label for the \eqn{Y}-axis, by default \code{Number of survivors}
#' @param main title for the plot
#' @param concentration a numeric value corresponding to some concentration(s) in
#' \code{data}. If \code{concentration = NULL}, draws a plot for each concentration
#' @param pool.replicate if \code{TRUE}, the datapoints of each replicate are
#' summed for a same concentration
#' @param addlegend if \code{TRUE}, adds a default legend to the plot
#' @param \dots Further arguments to be passed to generic methods
#'
#' @keywords plot
#'
#' @return a plot of class \code{ggplot}
#'
#' @examples
#' # (1) Load the data
#' data(zinc)
#' zinc <- survData(zinc)
#'
#' # (2) Plot survival data with a ggplot style
#' plot(zinc)
#'
#' # (3) Plot the survival data for one specific concentration
#' plot(zinc, concentration = 0.66)
#'
#' @import ggplot2
#' @importFrom stats aggregate
#'
#' @export
#'
plot.SurvData <- function(x,
                        xlab = "Time",
                        ylab = "Number of survivors",
                        main = NULL,
                        concentration = NULL,
                        pool.replicate = FALSE,
                        addlegend = FALSE, ...) {

    if (pool.replicate) {
        x <- cbind(stats::aggregate(Nsurv ~ time + conc, x, sum), replicate = 1)
    }
    if (!is.null(concentration)) {
        if (!(concentration %in% x$conc)) {
            stop("The argument [concentration] should correspond to
                 one of the tested concentrations")
        } else{
            x <- x[x$conc == concentration, ]
        }
    }

    if (length(unique(x$replicate)) == 1) {
        df <- ggplot(data = x, aes(x = time, y = Nsurv))
    } else {
        df <- ggplot(data = x, aes(x = time, y = Nsurv,
                            color = factor(replicate),
                            group = replicate))
    }

    fd <- df +
        theme_minimal() +
        geom_line() +
        geom_point() +
        ggtitle(main) +
        theme_minimal() +
        labs(x = xlab,
             y = ylab) +
        scale_color_hue("Replicate") +
        expand_limits(x = 0, y = 0) +
        facet_wrap(~ conc, ncol = 2)

    if (!addlegend) {
        fd <- fd + theme(legend.position = "none")
    }

    return(fd)
}



