#' @title Plot dose-response from \code{DoseResponse} objects
#'
#' @description
#' This is the generic \code{plot} S3 method for the \code{DoseResponse}
#' class. It plots the survival probability as a function of concentration at a given
#' target time.
#'
#' - For \code{SurvData} object: The function plots the observed values of
#' the survival probability at a given time point
#' as a function of concentration. The 95% binomial confidence interval is added
#' to each survival probability. It is calculated using function
#' \code{\link[stats]{binom.test}} from package \code{stats}.
#' Replicates are systematically pooled in this plot.
#'
#' - For \code{ReproData} object: The function plots the observed values of
#' the Number of   at a given time point
#' as a function of concentration. The 95% binomial confidence interval is added
#' to each survival probability. It is calculated using function
#' \code{poisson.test}.
#' Replicates are systematically pooled in this plot.
#'
#' - For \code{GrowthData} object: the function plots observed values of growth
#' at a given time point as a function of concentration. The 95% binomial
#' confidence interval is added
#' to each set of data at each concentration. It is calculated using function
#' \code{\link[stats]{t.test}} from package \code{stats}.
#'
#' @name DoseResponse
#'
#' @param x an object of class \code{SurvData}, \code{ReproData} or \code{GrowthData}
#' @param xlab a label for the \eqn{X}-axis, by default \code{Concentration}
#' @param ylab a label for the \eqn{Y}-axis, by default \code{Survival probability}
#' @param main main title for the plot
#' @param target.time a numeric value corresponding to some observed time in \code{data}
#' @param log.scale if \code{TRUE}, displays \eqn{X}-axis in log-scale
#' @param addlegend if \code{TRUE}, adds a default legend to the plot
#' @param \dots Further arguments to be passed to generic methods
#'
#' @note When \code{style = "generic"}, the function calls the generic function
#' \code{\link[graphics]{plot}}
#' @note When \code{style = "ggplot"}, the function return an object of class
#'  \code{ggplot}, see function \code{\link[ggplot2]{ggplot}}
#'
#'
#' @seealso \code{\link[stats]{binom.test}}
#'
#' @keywords plot
#'
#' @return a plot of class \code{ggplot}
#'
#' @examples
#'
#' library(ggplot2)
#'
#' # (1) Load the data
#' data(zinc)
#'
#' # (2) Create an object of class 'survData'
#' zinc <- survData(zinc)
#'
#' # (3) Plot dose-response
#' plotDoseResponse(zinc)
#'
#' # (4) Plot dose-respo nse with a generic style
#' plotDoseResponse(zinc, style = "generic")
#'
#' @import ggplot2
#' @importFrom grid arrow unit
#' @importFrom methods is
#' @importFrom stats aggregate
#'
#' @export
#'
plotDoseResponse <- function(x, ...){
    UseMethod("plotDoseResponse")
}



#' @rdname DoseResponse
#' @export
plot.DoseResponse <- function(x,
                              xlab = "Concentration",
                              ylab = NULL,
                              main = NULL,
                              log.scale = FALSE,
                              addlegend = TRUE,
                              ...) {

    x$legend_observation <- "Observed values"
    x$legend_binomial <- "Confidence intervals"

    fd <- ggplot(data = x) +
        theme_minimal() +
        geom_point(
            aes(x = conc, y = response, fill = legend_observation),
            col = "black") +
        geom_segment(
            aes(x = conc, xend = conc, y = qinf95, yend = qsup95,
                linetype = legend_binomial), col = "black") +
        scale_fill_hue("") +
        scale_linetype(name = "") +
        expand_limits(x = 0, y = 0) +
        ggtitle(main) +
        labs(x = xlab, y = ylab)

    if (log.scale) {
        fd <- fd + scale_x_log10()
    }
    if (!addlegend) {
        fd <- fd + theme(legend.position = "none")
    }
    return(fd)
}

#' @rdname DoseResponse
#' @export
plot.SurvDoseResponse <- function(x,
                                  xlab = "Concentration",
                                  ylab = NULL,
                                  main = NULL,
                                  log.scale = FALSE,
                                  addlegend = TRUE,
                                  ...){
    ylab = if (is.null(ylab)) { "Survival probability" }
    plot.DoseResponse(x, xlab, ylab, main, log.scale, addlegend, ...)
}
#' @rdname DoseResponse
#' @export
plot.ReproDoseResponse <- function(x,
                                  xlab = "Concentration",
                                  ylab = NULL,
                                  main = NULL,
                                  log.scale = FALSE,
                                  addlegend = TRUE,
                                  ...){
    ylab = if (is.null(ylab)) { "Reproduction rate" }
    plot.DoseResponse(x, xlab, ylab, main, log.scale, addlegend, ...)
}

#' @rdname DoseResponse
#' @export
plot.GrowthDoseResponse <- function(x,
                                   xlab = "Concentration",
                                   ylab = NULL,
                                   main = NULL,
                                   log.scale = FALSE,
                                   addlegend = TRUE,
                                   ...){
    ylab = if (is.null(ylab)) { "Growth measurement" }
    plot.DoseResponse(x, xlab, ylab, main, log.scale, addlegend, ...)
}
