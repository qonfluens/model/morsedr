#' @title Prediction base on fit object
#'
#' @export
#' @importFrom stats quantile
#'
predict.GrowthFitTT <- function(fit, display.conc = NULL){

    if (is.null(display.conc)) {
        display.conc <- fit$dataTT$conc
    }
    mctot <- do.call("rbind", fit$mcmc)
    # parameters
    d <- mctot[, "d"]
    sigma <- mctot[, "sigma"]
    log10b <- mctot[, "log10b"]
    b <- 10^log10b
    log10e <- mctot[, "log10e"]
    e <- 10^log10e
    ls <- lapply(display.conc, function(conc){
        d / (1 + (conc / e)^b)
    })
    df_mcmc <- as.data.frame(do.call("rbind", ls))
    df_quantile <- as.data.frame(
        t(apply(df_mcmc, 1, quantile,
                probs = c(0.025, 0.5, 0.975), na.rm = TRUE))
    )
    colnames(df_quantile) <- c("qinf95", "q50", "qsup95")

    return(list(display.conc = display.conc, mcmc = df_mcmc, quantile = df_quantile))
}
