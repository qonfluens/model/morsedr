#' @title Plotting method for \code{survFitTT} objects
#'
#' @description
#' This is the generic \code{plot} S3 method for the \code{survFitTT} class. It
#' plots concentration-response fit under target time survival analysis.
#'
#' The fitted curve represents the \strong{estimated survival probability} at
#' the target time as a function of the concentration of chemical compound;
#' When \code{adddata = TRUE} the black dots depict the \strong{observed survival
#' probability} at each tested concentration. Note that since our model does not take
#' inter-replicate variability into consideration, replicates are systematically
#' pooled in this plot.
#' The function plots both 95\% credible intervals for the estimated survival
#' probability (by default the grey area around the fitted curve) and 95\% binomial confidence
#' intervals for the observed survival probability (as black segments if
#' \code{adddata = TRUE}).
#' Both types of intervals are taken at the same level. Typically
#' a good fit is expected to display a large overlap between the two intervals.
#' If spaghetti = TRUE, the credible intervals are represented by two dotted
#' lines limiting the credible band, and a spaghetti plot is added to this band.
#' This spaghetti plot consists of the representation of simulated curves using parameter values
#' sampled in the posterior distribution (10\% of the MCMC chains are randomly
#' taken for this sample).
#'
#' @param x an object of class \code{survFitTT}
#' @param xlab a label for the \eqn{X}-axis, default is \code{Concentration}
#' @param ylab a label for the \eqn{Y}-axis, default is \code{Survival probability}
#' @param main main title for the plot
#' @param spaghetti if \code{TRUE}, the credible interval is represented by
#' multiple curves
#' @param adddata if \code{TRUE}, adds the observed data with confidence intervals
#' to the plot
#' @param addlegend if \code{TRUE}, adds a default legend to the plot
#' @param log.scale if \code{TRUE}, displays \eqn{X}-axis in log-scale
#' @param \dots Further arguments to be passed to generic methods
#'
#' @return a plot of class  \code{\link[ggplot2]{ggplot}}
#'
#' @examples
#'
#' # (1) Load the data
#' data(cadmium1)
#'
#' # (2) Create an object of class "survData"
#' dat <- survData(cadmium1)
#'
#' \donttest{
#' # (3) Run the survFitTT function with the log-logistic
#' #     binomial model
#' out <- survFitTT(dat, lcx = c(5, 10, 15, 20, 30, 50, 80),
#'                  quiet = TRUE)
#'
#' # (4) Plot the fitted curve
#' plot(out, log.scale = TRUE, adddata = TRUE)
#'
#' # (5) Plot the fitted curve
#' plot(out, xlab = expression("Concentration in" ~ mu~g.L^{-1}), adddata = TRUE)
#'
#' }
#'
#' @keywords plot
#'
#' @import grDevices
#' @import ggplot2
#' @importFrom gridExtra grid.arrange arrangeGrob
#' @importFrom stats aggregate
#'
#' @export
plot.SurvFitTT <- function(x,
                           xlab = "Concentration",
                           ylab = "Survival probability",
                           main = NULL,
                           display.conc = NULL,
                           spaghetti = FALSE,
                           adddata = FALSE,
                           addlegend = FALSE,
                           log.scale = FALSE, ...) {

    dataTT <- x$dataTT
    # pool data
    dataTT$Psurv <- dataTT$Nsurv / dataTT$Ninit
    # add binomial
    dataTT <- add_binomial(dataTT)
    dataTT$legend_CIobs = "Confidence intervals"
    dataTT$legend_observation = "Observed values"
    # display.conc
    if (is.null(display.conc)) {
        if (log.scale) {
            obs_conc <- dataTT$conc[dataTT$conc != 0]
            conc <- log10(obs_conc)
            s_conc <- seq(min(conc), max(conc), length.out = 100)
            display.conc <- 10^s_conc
        } else{
            obs_conc <- dataTT$conc
            s_conc <- seq(min(obs_conc), max(obs_conc), length.out = 100)
            display.conc <- s_conc
        }
    } else {
        obs_conc <- dataTT$conc
    }
    # compute predictions
    predictTT <- predict(x, display.conc)

    df_quantile <- predictTT$quantile
    df_quantile$conc <- predictTT$display.conc

    df_quantile$legend_median = "loglogistic"
    df_quantile$legend_CI = "Confident interval"
    plt <- ggplot() +
        theme_minimal() +
        labs(x = xlab, y = ylab, title = main) +
        scale_shape_manual(name = "", values = 19) +
        scale_color_manual(name = "", values = "black") +
        scale_linetype_manual(name = "",
            values = c("loglogistic" = 1, "Confident interval" = 2))

    # 1. spaghetti
    if (spaghetti) {
        len_mcmc <- length(predictTT$mcmc[[1]])
        sample_range <- sample(1:len_mcmc, 500, replace = FALSE)
        ls_spaghetti <- lapply(seq_along(predictTT$mcmc), function(i){
            sampled <- predictTT$mcmc[[i]][sample_range]
            data.frame(
                Psurv = sampled,
                line = 1:length(sampled),
                conc = predictTT$display.conc[[i]])
        })
        df_spaghetti <- as.data.frame(do.call("rbind", ls_spaghetti))

        plt <- plt +
            geom_line(data = df_spaghetti,
                        aes(x = conc, y = Psurv, group = line),
                        color = "grey",  alpha = 0.1)

    } else{
        plt <- plt +
            geom_ribbon(data = df_quantile,
                        aes(x = conc, ymin = qinf95, ymax = qsup95),
                        linetype = 2, fill = "grey", alpha = 0.5, color = NA)
    }
    # 2. median line
    plt <- plt +
        geom_line(data = df_quantile,
                  aes(x = conc, y = q50, linetype = legend_median),
                  color = "orange") +
        geom_line(data = df_quantile,
                  aes(x = conc, y = qinf95, linetype = legend_CI),
                  color = "orange") +
        geom_line(data = df_quantile,
                  aes(x = conc, y = qsup95, linetype = legend_CI),
                  color = "orange")
    # 3. log-scaling
    if (log.scale) {
        plt <- plt + scale_x_log10(breaks = obs_conc)
    } else {
        plt <- plt + scale_x_continuous(breaks = obs_conc)
    }

    # 4. add data
    if (adddata) {
        plt <- plt +
            geom_point(data = dataTT,
                       aes(x = conc, y = Psurv,
                           shape = legend_observation),
                       color = "black") +
            geom_linerange(
                data = dataTT,
                aes(x = conc, ymin = qinf95, ymax = qsup95,
                    color = legend_CIobs))
    }
    # 5. legend
    if (!addlegend) {
        plt <- plt + theme(legend.position = "none")
    }
    return(plt)
}
