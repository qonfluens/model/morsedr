#' @importFrom stats quantile
#' @importFrom stats aggregate
#' @importFrom stats binom.test
#'
add_binomial <- function(df){
    # compute binomial
    ls_binom = lapply(1:nrow(df), function(r) {
        binom.test(df[r,][["Nsurv"]], df[r,][["Ninit"]])$conf.int
    })
    df_binom = do.call("rbind", ls_binom)
    colnames(df_binom) = c("qinf95", "qsup95")
    df_out <- data.frame(df, df_binom)
    return(df_out)
}

#' @importFrom stats t.test
#'
add_t_test <- function(df){
    subdata <- split(df, list(df$time, df$conc), drop = TRUE)
    # compute t.test
    ls_t_test = lapply(subdata, function(r) {
        m = t.test(r[["growth"]])$conf.int
        do.call(rbind, replicate(nrow(r), m, simplify = FALSE))
    })
    df_t_test = do.call("rbind", ls_t_test)
    colnames(df_t_test) = c("qinf95", "qsup95")
    df_out <- data.frame(df, df_t_test)
    return(df_out)
}

#' @importFrom stats poisson.test
#'
add_poisson <- function(df){
    # compute poisson
    ls_pois = lapply(1:nrow(df), function(r) {
        poisson.test(df[r,][["Nreprocumul"]], df[r,][["Nindtime"]])$conf.int
    })
    df_pois = do.call("rbind", ls_pois)
    colnames(df_pois) = c("qinf95", "qsup95")
    df_out <- data.frame(df, df_pois)
    return(df_out)
}

#' @title Test if \code{x} is between \code{min} and \code{max}
#'
is.between <- function(x, min, max){ (x >= min & x <= max) }

#' @title filter dataset at target time
#'
#' @description
#' filter the dataset reproData or survData on the target time selected.
#'
#' @param data an object of class reproData or survData
#' @param target.time the time targetted to consider as the last time for the analysis
#'
#' @return subset the dataframe for target.time used by function with target.time arg
#'
selectDataTT <- function(data, target.time = NULL) {
    if (is.null(target.time)) {
        target.time <- max(data$time)
    }
    if (!any(data$time == target.time) || target.time == 0) {
        stop("target.time is not one of the possible time !")
    }
    dataTT <- data[data$time == target.time, ]
    return(dataTT)
}

#' @title Estimated ECx and 95 CIs
#'
#' @description
#' create the table of estimated values of LCx or ECxfor the survival analyses
#'
#' @param mcmc list of estimated parameters for the model with each item
#'  representing a chains
#' @param xcx vector of values of LCx or ECX
#' @param varx character string for lcx or ecx
#'
#' @return a data.frame with the estimated ECx and their CIs
#' 95% (3 columns (values, CIinf, CIsup) and length(x) rows)
#'
#' @importFrom stats quantile
#'
estimXCX <- function(mcmc, xcx, varx) {

    # Retrieving estimated parameters of the model
    mctot <- do.call("rbind", mcmc)
    b <- 10^mctot[, "log10b"]
    e <- 10^mctot[, "log10e"]

    # Calculation XCx median and quantiles
    XCx <- sapply(xcx, function(x) {e * ((100 / (100 - x)) - 1)^(1 / b)})

    q50 <- apply(XCx, 2, function(x) {quantile(x, probs = 0.5)})
    qinf95 <- apply(XCx, 2, function(x) {quantile(x, probs = 0.025)})
    qsup95 <- apply(XCx, 2, function(x) {quantile(x, probs = 0.975)})

    # defining names
    XCname <- sapply(xcx, function(x) {paste(varx, x, sep = '')})

    # create the dataframe with ECx median and quantiles
    res <- data.frame(
        median = q50,
        Q2.5 = qinf95,
        Q97.5 = qsup95,
        row.names = XCname)

    return(res)
}


#' @title Calculate the dic for a jags model
#'
#' @description
#' Compute the DIC using the \code{\link[rjags]{dic.samples}} function
#'
#'
#' @param m.M jags model object
#' @param sampling.parameters a \code{list} of parameters to compute DIC
#' - \code{niter}: number of iteration (mcmc)
#' - \code{thin}: thining rate parameter
#' - \code{burnin}: number of iteration burned
#' @param quiet silent option
#'
#' @return DIC computed with \code{\link[rjags]{dic.samples}}
#'
#' @import rjags
calcDIC <- function(m.M, sampling.parameters, quiet) {
    prog.b <- ifelse(quiet == TRUE, "none", "text")
    dic <- rjags::dic.samples(m.M,
                              n.iter = sampling.parameters$niter,
                       thin = sampling.parameters$thin,
                       progress.bar = prog.b)

    # return penalised DIC
    penaliser_dic <- round(sum(sapply(dic$deviance, mean) + sapply(dic$penalty, mean)))
    return(penaliser_dic)
}

#' @title Estimate the number of iteration
#'
#' @description
#' estimate the number of iteration required for the estimation
#' by using the \code{[coda]raftery.diag}, default value is 3746.
#'
#' @param model jags model from loading function
#' @param parameters parameters from loading function
#' @param n.chains the number of parallel chains for the model
#' @param quiet silent control
#'
#' @returns
#' - \code{niter}: number of iteration (mcmc)
#' - \code{thin}: thining rate parameter
#' - \code{burnin}: number of iteration burned
#'
#' @import rjags
#' @importFrom coda raftery.diag
#'
modelSamplingParameters <- function(model, parameters, n.chains, quiet) {
    niter.init <- 5000
    prog.b <- ifelse(quiet == TRUE, "none", "text") # plot progress bar option
    mcmc <- rjags::coda.samples(
        model, parameters, n.iter = niter.init,
        thin = 1, progress.bar = prog.b)
    RL <- raftery.diag(mcmc)

    # check raftery.diag result (number of sample for the diagnostic procedure)
    if (n.chains < 2) {
        stop('2 or more parallel chains required !')
    }
    # extract raftery diagnostic results
    resmatrix <- RL[[1]]$resmatrix
    for (i in 2:length(RL)) {
        resmatrix <- rbind(resmatrix, RL[[i]]$resmatrix)
    }

    # creation of sampling parameters
    thin <- round(max(resmatrix[, "I"]) + 0.5) # autocorrelation
    niter <- max(resmatrix[, "Nmin"]) * thin # number of iteration
    burnin <- max(resmatrix[, "M"]) # burnin period

    return(list(niter = niter, thin = thin, burnin = burnin))
}
