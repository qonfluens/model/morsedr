#' @title Plotting method for \code{GrowthData} objects
#'
#' @description
#' This is the generic \code{plot} S3 method for the \code{GrowthData} class.
#' It plots the growth data as a function of time by concentration panels.
#'
#' @param x an object of class \code{GrowthData}
#' @param xlab label of the \eqn{X}-axis
#' @param ylab label of the \eqn{Y}-axis, by default \code{Growth measurement}
#' @param main main title for the plot
#' @param concentration a numeric value corresponding to some concentration in
#' \code{data}. If \code{concentration = NULL}, draws a plot for each concentration
#' @param addlegend if \code{TRUE}, adds a default legend to the plot
#' @param \dots Further arguments to be passed to generic methods
#'
#'
#' @keywords plot
#'
#' @return a plot of class \code{ggplot}
#'
#' @examples
#' # (1) Load the data
#' data(cadmium_daphnia)
#'
#' # (2) Create an object of class 'GrowthData'
#' gCd <- growthData(cadmium_daphnia)
#'
#' # (3) Plot the growth data
#' plot(gCd)
#'
#' # (4) Plot the growth data for a fixed concentration
#' plot(gCd, concentration = 25)
#'
#'
#' @export
plot.GrowthData <- function(x,
                           xlab = "Time",
                           ylab = "Growth measurement",
                           main = NULL,
                           concentration = NULL,
                           addlegend = FALSE, ...) {
    if (!is.null(concentration)) {
        if (!(concentration %in% x$conc)) {
            stop("The argument [concentration] should correspond to
                 one of the tested concentrations")
        } else{
            x <- x[x$conc == concentration, ]
        }
    }

    if (length(unique(x$conc)) == 1) {
        df <- ggplot(data = x, aes(x = time, y = growth))
    } else {
        df <- ggplot(data = x, aes(x = time, y = growth,
                                   color = factor(conc),
                                   group = conc))
    }

    fd <- df +
        theme_minimal() +
        geom_point() +
        ggtitle(main) +
        theme_minimal() +
        labs(x = xlab,
             y = ylab) +
        scale_color_hue("Concentration") +
        expand_limits(x = 0, y = 0) +
        facet_wrap(~ conc, ncol = 2)

    if (!addlegend) {
        fd <- fd + theme(legend.position = "none")
    }

    return(fd)
}

