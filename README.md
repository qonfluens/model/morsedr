# morseDR

## package coverage:

From R session

```R
library(covr)
cov <- package_coverage("morseDR")
```

# Style of process

## The succession of steps

1. `data`: load the data set.
2. `survData` or `reproDate`: make a `SurvData`and `ReproData` object. 
3. These objects inherit of `data.frame` ??
4. `plot`: plot a `SurvData` or `ReproData`.
5. `summary`:
6. `print`:
7. `doseResponse`: return a `DoseResponse` object.
8. `plot`: plot a `DoseResponse` object.
8. `fit`: fit a `SurvData` or `ReproData` and return a `SurvFit` or `ReproFit` object.
9. `plot`: plot a `SurvFit` or `ReproFit` object.
10. `ppc`: return a `PPC` object. ???
11. `plot`: plot a `PPC` object.


# Coding Style

Object: `BigCamelCase`

```R
class(x) <- append("ObjectCamelCase", class(x))
```

Methods: `small_snake_case`

```R
methods_snake_case.ObjectCamelCase <- function(...){}
```

Function (no methods - not linked to object): `smallCamelCase`

```R
smallCamelCase <- function(...){}
```
